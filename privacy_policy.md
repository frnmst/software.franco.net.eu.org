# Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Information](#information)
  - [Privacy policy](#privacy-policy)
    - [Onion](#onion)
  - [Cookie policy](#cookie-policy)
    - [Legal](#legal)
  - [Accounts](#accounts)
    - [GitHub OAuth2 authentication](#github-oauth2-authentication)
    - [Traditional accounts](#traditional-accounts)
  - [Notes on email](#notes-on-email)

<!--TOC-->

# Information

This website serve as the main source of my repositories.

## Privacy policy

IP addresses are collected by an Apache HTTP server which acts as reverse proxy and
stored in a log file in this form:


    ${IP} - - [${dd}/${mm}/${yyyy}:${hh}:${mm}:${ss} ${timezone}] "${http_operation} ${resource} " ${HTTP_code} ${other}

Variables are marked with `${}`.

These logs are rotated daily and kept for 7 days.
The reason for this is to enable an abuse protection system through [Fail2Ban](https://www.fail2ban.org/wiki/index.php/Main_Page).

I have NO interest in:
- selling this information
- tracking users
- etc...

### Onion

To protect your privacy, as an alternative,
an onion address is available for this instance: http://jb2xdimn4cuoty3igqh3qknyt7ls6hwswzwl7xmieheemsjuirvqcaad.onion

Bare in mind that git cloning over HTTP has been disabled for the moment because
my instance of Fail2Ban is configured to ignore localhost. All traffic from TOR
is infact marked as coming from localhost by Apache and Gitea, for example:

    127.0.0.1 - - [08/Mar/2021:10:03:02 +0100] "GET /user/avatar/frnmst/290 HTTP/1.1" 302 64

If you want to clone repositories through TOR it is sufficient to use torsocks. For example:

    torsocks --isolate git clone https://software.franco.net.eu.org/frnmst/blog.git

## Cookie policy

This website does NOT use:

- third party cookies
- statistics cookies
- profilation cookies
- marketing cookies

This website uses:

- technical cookies

| Technical cookie name    |
|--------------------------|
| `_csrf`                  |
| `i_like_gitea`           |
| `lang`                   |

### Legal

- https://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/3167654 Article 1 comma 1-a.
- https://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/4006878

## Accounts

To create a new account the only information needed are:

- a username
- an email
- a password which not readable by the administrator in plaintext form

For the moment, repository creation is disabled.

An account gives you the possibility to interact with the issue trackers on
this instance.

### GitHub OAuth2 authentication

You can create an account using GitHub
at https://software.franco.net.eu.org/user/oauth2/software.franco

If you select this type of authentication GitHub
[terms](https://docs.github.com/en/github/site-policy/github-terms-of-service)
and
[privacy policy](https://docs.github.com/en/github/site-policy/github-privacy-statement)
apply.

Please note that you cannot change the username once the account is created because
this type of authentication is external. To change username you must first delete
your account and then create a new one.

### Traditional accounts

Traditional registration is disabled to avoid spammers,
however if you want an account of this type you can contact me.

## Notes on email

This server is hosted on a dynamic IP address. Even though I have set up a
mail server with SPF, DKIM, etc... other email providers mark me as spam
because of that.

As you know, web app git services such as GitHub, GitLab, Gitea, etc... rely
on email for some notifications. If you decide to have an account here then
you have two possibilities:

- expect to have email notifications broken
- ask me to add an email to be used just for this service which you can use
  through IMAP and SMTP
